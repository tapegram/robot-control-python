# robot-control-python
Robot Controller Kata in Python

## Dev Setup
### Setup a virtual environment at the root of the project
```bash
python3 -m venv venv
```

### Activate the virtual env
```bash
source venv/bin/activate
```
or if you use fish like me
```fish
source venv/bin/activate.fish
```

### Install all the dependencies
```bash
pip install -r requirements.txt
```

## Testing
### Lint
`pylint src/*.py`

### Test
`pytest`

or if you are feeling frisky you can run `ptw` to run pytest-watcher and see your efforts fail in real time!

## CI
See [this file](.gitlab-ci.yml)


## Running the actual app
There is a very small CLI client implemented with [Click](https://click.palletsprojects.com/en/7.x/).

You can use this client by running the following from the root of this package.
```bash
python robot-control.py --file example-input.txt
```

You can also ask it for help
```bash
python robot-control.py --help
```
