import click

from src.controllers.file.handle_file import handle_file


"""
This is a CLI client to Robot Control kata
"""


@click.command()
@click.option(
    "--file",
    prompt="Path to your instruction file",
    help="The path to the instruction file to run our little robot simulation!",
)
def run(file):
    print(handle_file(file))


if __name__ == "__main__":
    run()