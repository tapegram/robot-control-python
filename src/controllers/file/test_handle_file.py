import os
import sys

from src.controllers.file.handle_file import handle_file


def test_with_example():
    assert (
        handle_file(os.path.join(sys.path[0], "src/controllers/file/example.txt"))
        == "S 4 99"
    )
