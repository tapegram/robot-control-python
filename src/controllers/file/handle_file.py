from src.domain.coord import Coord
from src.domain.robot import Robot
from src.domain.direction import Direction, East, North, South, West
from src.controllers.file.file_input import FileInput
from src.service.handle_commands import handle_commands


def handle_file(path) -> str:
    input = FileInput.from_file(path)
    state = handle_commands(
        initialRobotPosition=input.initial_location,
        initialRobotDirection=input.initial_direction,
        upperBoundX=100,
        upperBoundY=100,
        commands=input.commands,
    )

    return to_result(state.robot)


def to_result(robot: Robot) -> str:
    direction = to_direction(robot.direction)
    location = to_location(robot.location)
    return "{} {}".format(direction, location)


def to_direction(direction: Direction) -> str:
    if isinstance(direction, North):
        return "N"
    if isinstance(direction, East):
        return "E"
    if isinstance(direction, South):
        return "S"
    if isinstance(direction, West):
        return "W"

    raise Exception("Unknown Direction Found")


def to_location(location: Coord) -> str:
    return "{} {}".format(location.x, location.y)