import attr
import re

from src.domain.direction import North, West, South, East
from src.domain.coord import Coord
from src.service.command import Command


@attr.s(frozen=True)
class FileInput:
    initial_direction = attr.ib()
    initial_location = attr.ib()
    commands = attr.ib()

    @staticmethod
    def from_file(path: str):
        input = open(path, "r").readlines()
        return FileInput(
            initial_direction=to_initial_direction(input),
            initial_location=to_initial_location(input),
            commands=to_commands(input),
        )


def to_initial_direction(input):
    direction = input[0].split(" ")[0].upper()
    if direction == "N":
        return North()
    if direction == "E":
        return East()
    if direction == "S":
        return South()
    if direction == "W":
        return West()

    raise Exception("Invalid Starting Direction")


def to_initial_location(input):
    coords = input[0].split(" ")[1:]
    return Coord(int(coords[0]), int(coords[1]))


def to_commands(input):
    commands = []
    raw_commands = input[1].upper().strip()
    list_raw_commands = re.findall(r"[A-Z]\d*", raw_commands)

    for raw_command in list_raw_commands:
        commands.extend(_to_commands(raw_command))

    return commands


def _to_commands(raw_command):
    """
    Assuming that raw commands are a combination of an instruction (M, R, or L)
    and an option count (as an int). If there is no count, we default to 1.

    Examples: M, M1, M50, R, R7, L999

    Returns: List<Command>
    """
    count = 1
    if len(raw_command) > 1:
        count = int(raw_command[1:])

    instruction = raw_command[0]

    if instruction == "M":
        command_fn = Command.forward
    elif instruction == "R":
        command_fn = Command.right
    elif instruction == "L":
        command_fn = Command.left
    else:
        raise Exception("Unrecognized instruction: " + instruction)

    return [command_fn() for _ in range(count)]
