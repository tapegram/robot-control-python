import attr

from src.domain.coord import Coord


@attr.s(frozen=True)
class Robot:
    direction = attr.ib()
    location = attr.ib()

    def right(self):
        return attr.evolve(self, direction=self.direction.right())

    def left(self):
        return attr.evolve(self, direction=self.direction.left())

    def move(self, coord: Coord):
        return attr.evolve(self, location=coord)

    def calculate_move_forward(self) -> Coord:
        return self.direction.apply(self.location, steps=1)