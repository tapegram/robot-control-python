import attr

from src.domain.coord import Coord


@attr.s(frozen=True)
class World:
    x_bound = attr.ib(default=100)
    y_bound = attr.ib(default=100)

    def wrap(self, coord: Coord) -> Coord:
        return attr.evolve(
            coord,
            x=coord.x % self.x_bound,
            y=coord.y % self.y_bound,
        )
