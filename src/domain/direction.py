import attr
import abc

from src.domain.coord import Coord


class Direction(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def left(self):
        pass

    @abc.abstractmethod
    def right(self):
        pass

    @abc.abstractmethod
    def apply(self, coord: Coord, steps: int):
        pass


@attr.s(frozen=True)
class North(Direction):
    def left(self):
        return West()

    def right(self):
        return East()

    def apply(self, coord: Coord, steps: int):
        return attr.evolve(coord, y=coord.y + steps)


@attr.s(frozen=True)
class East(Direction):
    def left(self):
        return North()

    def right(self):
        return South()

    def apply(self, coord: Coord, steps: int):
        return attr.evolve(coord, x=coord.x + steps)


@attr.s(frozen=True)
class South(Direction):
    def left(self):
        return East()

    def right(self):
        return West()

    def apply(self, coord: Coord, steps: int):
        return attr.evolve(coord, y=coord.y - steps)


@attr.s(frozen=True)
class West(Direction):
    def left(self):
        return South()

    def right(self):
        return North()

    def apply(self, coord: Coord, steps: int):
        return attr.evolve(coord, x=coord.x - steps)