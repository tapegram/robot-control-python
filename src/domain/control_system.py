import attr


@attr.s(frozen=True)
class ControlSystem:
    world = attr.ib()
    robot = attr.ib()

    def move_forward(self):
        # Would be nice to use a pipe operator here
        proposed_coord = self.robot.calculate_move_forward()
        wrapped_coord = self.world.wrap(proposed_coord)
        robot = self.robot.move(wrapped_coord)

        return attr.evolve(self, robot=robot)

    def rotate_left(self):
        return attr.evolve(self, robot=self.robot.left())

    def rotate_right(self):
        return attr.evolve(self, robot=self.robot.right())