class RobotMustBePlacedOnTheBoard(Exception):
    pass


class WorldMustHavePostiveIntsAsBoundaries(Exception):
    pass