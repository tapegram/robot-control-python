import attr


@attr.s(frozen=True)
class Coord:
    x = attr.ib(default=0)
    y = attr.ib(default=0)