import pytest

from src.domain.control_system import ControlSystem
from src.domain.coord import Coord
from src.domain.exceptions import (
    RobotMustBePlacedOnTheBoard,
    WorldMustHavePostiveIntsAsBoundaries,
)
from src.domain.direction import North, West
from src.domain.robot import Robot
from src.domain.world import World
from src.service.handle_commands import handle_commands


def test_with_no_commands():
    """
    Given a valid initial state
    When no commands are passed in
    Then the result should be an initialized control system
    """
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=North(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=North(),
            location=Coord(0, 0),
        ),
    )
    assert handle_commands(
        initialRobotPosition=Coord(50, 21),
        initialRobotDirection=West(),
        upperBoundX=70,
        upperBoundY=50,
        commands=[],
    ) == ControlSystem(
        world=World(x_bound=70, y_bound=50),
        robot=Robot(
            direction=West(),
            location=Coord(50, 21),
        ),
    )


def test_with_invalid_robot_placement():
    with pytest.raises(RobotMustBePlacedOnTheBoard):
        handle_commands(
            initialRobotPosition=Coord(100, 0),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=100,
            commands=[],
        )
    with pytest.raises(RobotMustBePlacedOnTheBoard):
        handle_commands(
            initialRobotPosition=Coord(5, 900),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=100,
            commands=[],
        )
    with pytest.raises(RobotMustBePlacedOnTheBoard):
        handle_commands(
            initialRobotPosition=Coord(-1, 50),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=100,
            commands=[],
        )
    with pytest.raises(RobotMustBePlacedOnTheBoard):
        handle_commands(
            initialRobotPosition=Coord(75, -1),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=100,
            commands=[],
        )
    with pytest.raises(RobotMustBePlacedOnTheBoard):
        handle_commands(
            initialRobotPosition=Coord(500, -10),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=100,
            commands=[],
        )
    with pytest.raises(RobotMustBePlacedOnTheBoard):
        handle_commands(
            initialRobotPosition=Coord(-500, -10),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=100,
            commands=[],
        )
    with pytest.raises(RobotMustBePlacedOnTheBoard):
        handle_commands(
            initialRobotPosition=Coord(100, 100),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=100,
            commands=[],
        )


def test_with_invalid_world_boundaries():
    with pytest.raises(WorldMustHavePostiveIntsAsBoundaries):
        handle_commands(
            initialRobotPosition=Coord(99, 0),
            initialRobotDirection=North(),
            upperBoundX=-1,
            upperBoundY=100,
            commands=[],
        )
    with pytest.raises(WorldMustHavePostiveIntsAsBoundaries):
        handle_commands(
            initialRobotPosition=Coord(99, 0),
            initialRobotDirection=North(),
            upperBoundX=100,
            upperBoundY=-1,
            commands=[],
        )
    with pytest.raises(WorldMustHavePostiveIntsAsBoundaries):
        handle_commands(
            initialRobotPosition=Coord(99, 0),
            initialRobotDirection=North(),
            upperBoundX=-1,
            upperBoundY=-1,
            commands=[],
        )
    with pytest.raises(WorldMustHavePostiveIntsAsBoundaries):
        handle_commands(
            initialRobotPosition=Coord(99, 0),
            initialRobotDirection=North(),
            upperBoundX=-11,
            upperBoundY=50,
            commands=[],
        )
    with pytest.raises(WorldMustHavePostiveIntsAsBoundaries):
        handle_commands(
            initialRobotPosition=Coord(99, 0),
            initialRobotDirection=North(),
            upperBoundX=50,
            upperBoundY=-11,
            commands=[],
        )
