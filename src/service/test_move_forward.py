from src.domain.control_system import ControlSystem
from src.domain.coord import Coord
from src.domain.direction import North, West, East, South
from src.domain.robot import Robot
from src.domain.world import World
from src.service.command import Command
from src.service.handle_commands import handle_commands


def test_facing_north():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=North(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=North(),
            location=Coord(0, 1),
        ),
    )


def test_facing_east():
    assert handle_commands(
        initialRobotPosition=Coord(50, 50),
        initialRobotDirection=East(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=East(),
            location=Coord(51, 50),
        ),
    )


def test_facing_south():
    assert handle_commands(
        initialRobotPosition=Coord(50, 50),
        initialRobotDirection=South(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=South(),
            location=Coord(50, 49),
        ),
    )


def test_facing_west():
    assert handle_commands(
        initialRobotPosition=Coord(50, 50),
        initialRobotDirection=West(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=West(),
            location=Coord(49, 50),
        ),
    )


def test_wrap_north():
    assert handle_commands(
        initialRobotPosition=Coord(50, 99),
        initialRobotDirection=North(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=North(),
            location=Coord(50, 0),
        ),
    )


def test_wrap_east():
    assert handle_commands(
        initialRobotPosition=Coord(99, 99),
        initialRobotDirection=East(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=East(),
            location=Coord(0, 99),
        ),
    )


def test_wrap_south():
    assert handle_commands(
        initialRobotPosition=Coord(99, 0),
        initialRobotDirection=South(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=South(),
            location=Coord(99, 99),
        ),
    )


def test_wrap_west():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=West(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=West(),
            location=Coord(99, 0),
        ),
    )