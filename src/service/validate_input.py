from typing import List

from src.domain.exceptions import (
    RobotMustBePlacedOnTheBoard,
    WorldMustHavePostiveIntsAsBoundaries,
)
from src.domain.coord import Coord
from src.domain.direction import Direction
from src.service.command import Command


def validate_input(f):
    def _validate_input(
        initialRobotPosition: Coord,
        initialRobotDirection: Direction,
        upperBoundX: int,
        upperBoundY: int,
        commands: List[Command],
    ):
        # As we iterate, it may make sense to differentiate between input validation
        # and errors thrown in the domain. Though ideally the domain is implemented in such a way
        # that it can never reach an invalid state.
        if upperBoundX < 1:
            raise WorldMustHavePostiveIntsAsBoundaries
        if upperBoundY < 1:
            raise WorldMustHavePostiveIntsAsBoundaries
        if initialRobotPosition.x < 0 or initialRobotPosition.y < 0:
            raise RobotMustBePlacedOnTheBoard
        if (
            initialRobotPosition.x >= upperBoundX
            or initialRobotPosition.y >= upperBoundY
        ):
            raise RobotMustBePlacedOnTheBoard

        return f(
            initialRobotPosition,
            initialRobotDirection,
            upperBoundX,
            upperBoundY,
            commands,
        )

    return _validate_input