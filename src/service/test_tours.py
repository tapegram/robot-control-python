from src.domain.control_system import ControlSystem
from src.domain.coord import Coord
from src.domain.direction import North, West, South
from src.domain.robot import Robot
from src.domain.world import World
from src.service.command import Command
from src.service.handle_commands import handle_commands


def test_circle_the_world_clockwise():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=North(),
        upperBoundX=5,
        upperBoundY=5,
        commands=[
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.right(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.right(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.right(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=5, y_bound=5),
        robot=Robot(
            direction=West(),
            location=Coord(0, 0),
        ),
    )


def test_circle_the_world_counter_clockwise():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=North(),
        upperBoundX=5,
        upperBoundY=5,
        commands=[
            Command.right(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.left(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.left(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.left(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=5, y_bound=5),
        robot=Robot(
            direction=South(),
            location=Coord(0, 0),
        ),
    )


def test_staircase():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=North(),
        upperBoundX=5,
        upperBoundY=5,
        commands=[
            Command.forward(),
            Command.right(),
            Command.forward(),
            Command.left(),
            Command.forward(),
            Command.right(),
            Command.forward(),
            Command.left(),
            Command.forward(),
            Command.right(),
            Command.forward(),
            Command.left(),
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=5, y_bound=5),
        robot=Robot(
            direction=North(),
            location=Coord(3, 4),
        ),
    )


def test_tour1():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=North(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.forward(),
            Command.right(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.forward(),
            Command.left(),
            Command.left(),
            Command.left(),
            Command.forward(),
            Command.forward(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=South(),
            location=Coord(4, 99),
        ),
    )
