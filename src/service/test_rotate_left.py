from src.domain.control_system import ControlSystem
from src.domain.coord import Coord
from src.domain.direction import North, West, East, South
from src.domain.robot import Robot
from src.domain.world import World
from src.service.command import Command
from src.service.handle_commands import handle_commands


def test_from_north():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=North(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.left(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=West(),
            location=Coord(0, 0),
        ),
    )


def test_from_east():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=East(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.left(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=North(),
            location=Coord(0, 0),
        ),
    )


def test_from_south():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=South(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.left(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=East(),
            location=Coord(0, 0),
        ),
    )


def test_from_west():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=West(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.left(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=South(),
            location=Coord(0, 0),
        ),
    )


def test_270_from_west():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=West(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.left(),
            Command.left(),
            Command.left(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=North(),
            location=Coord(0, 0),
        ),
    )


def test_360_from_east():
    assert handle_commands(
        initialRobotPosition=Coord(0, 0),
        initialRobotDirection=East(),
        upperBoundX=100,
        upperBoundY=100,
        commands=[
            Command.left(),
            Command.left(),
            Command.left(),
            Command.left(),
        ],
    ) == ControlSystem(
        world=World(x_bound=100, y_bound=100),
        robot=Robot(
            direction=East(),
            location=Coord(0, 0),
        ),
    )
