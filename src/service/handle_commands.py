from typing import List

from src.domain.control_system import ControlSystem
from src.domain.coord import Coord
from src.domain.direction import Direction
from src.domain.robot import Robot
from src.domain.world import World
from src.service.command import Command, Instruction
from src.service.validate_input import validate_input


@validate_input
def handle_commands(
    initialRobotPosition: Coord,
    initialRobotDirection: Direction,
    upperBoundX: int,
    upperBoundY: int,
    commands: List[Command],
):
    control_system = ControlSystem(
        world=World(x_bound=upperBoundX, y_bound=upperBoundY),
        robot=Robot(
            direction=initialRobotDirection,
            location=initialRobotPosition,
        ),
    )

    for command in commands:
        if command.instruction == Instruction.MOVE_FORWARD:
            control_system = control_system.move_forward()
        elif command.instruction == Instruction.LEFT_90:
            control_system = control_system.rotate_left()
        elif command.instruction == Instruction.RIGHT_90:
            control_system = control_system.rotate_right()
        else:
            pass

    return control_system
