import attr
from enum import Enum


@attr.s(frozen=True)
class Command:
    instruction = attr.ib()

    @staticmethod
    def forward():
        return Command(
            instruction=Instruction.MOVE_FORWARD,
        )

    @staticmethod
    def left():
        return Command(
            instruction=Instruction.LEFT_90,
        )

    @staticmethod
    def right():
        return Command(
            instruction=Instruction.RIGHT_90,
        )


# Maybe rotating should be more configurable than 90 degrees in the future.
class Instruction(Enum):
    MOVE_FORWARD = 1
    LEFT_90 = 2
    RIGHT_90 = 3
